###########################################################################
#
# NAME: EnggAD.psm1
#
# AUTHOR:  Curtis Oliver
#
# COMMENT: 
#
# VERSION HISTORY:
# 1.0 01/21/2020 - Initial release
#
###########################################################################
Import-Module -Name ActiveDirectory

$Public = Get-ChildItem -Path $PSScriptRoot\Public -Filter *.ps1 | ForEach-Object {
    . $_.FullName
    ($_.Name).TrimEnd('.ps1')
}

Get-ChildItem -Path $PSScriptRoot\Private -Filter *.ps1 | ForEach-Object {
    . $_.FullName
}

$Variables = @()

Export-ModuleMember -Function $Public -Variable $Variables
