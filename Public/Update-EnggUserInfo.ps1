﻿<#
.SYNOPSIS
   Updates AD user attributes
.DESCRIPTION
   Updates AD user attributes from results returned from Get-EnggUserInfo function. Attributes updated are:
    
    **EnggRole (Have to extend schema to add this attribute... still debating this in my head)
    **Department (Administration, Centre for Cooperative Education, Development, Finance, Human Resources, Information Technology, Marketing, Research, Safety, Shop, Senior Administration, Student Services Graduate, Student Services Undergraduate, Web Development)
    **Division (Biomechanical Engineering, Civil and Environmental Engineering, Chemical and Materials Engineering, Electrical and Computer Engineering, Faculty, Mechanical Engineering)
    *Office
    *OfficePhone
    *Title

    **These are not yet implemented
.PARAMETER UserInfo
   This is the object returned from the Get-EnggUserInfo function.
.PARAMETER Attributes
   This allows you to set only certain attributes.
.PARAMETER Server
   Domain controller to perform the update on.
.EXAMPLE
   Get-EnggUserInfo -Ccid cdoliver | Update-EnggUserInfo

   Updates AD attributes of user from piped in results.
.EXAMPLE
   Update-EnggUserInfo -UserInfo (Get-EnggUserInfo -Ccid cdoliver)

   Same as above, without the pipeline.
.EXAMPLE
   Get-EnggUserInfo -Ccid cdoliver | Update-EnggUserInfo -Attributes Department,OfficePhone

   Updates only the department and officephone attributes of user.
.NOTES
   Author: Curtis D. Oliver
   Created: March 02, 2020
#>
function Update-EnggUserInfo {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true,
                   Position=0,
                   ValueFromPipeline=$true,
                   HelpMessage="Pipe in results from Get-EnggUserInfo, or define as (Get-EnggUserInfo -Ccid <ccid>)")]
        [Alias("Name","UserName","Id")]
        [PSObject[]]$UserInfo,
        [ValidateSet("All","EnggRole","Department","Division","Office","OfficePhone","Title")]
        [string[]]$Attributes = "All",
        [string]$Server = "dc1.engineering.ualberta.ca"
    )

    begin {}

    process {
        $UserInfo | ForEach-Object {
            if ( $Attributes -eq "All" ) {
                Set-ADUser -Identity $_.Ccid -Department $_.Department -Office $_.Office -OfficePhone $_.OfficePhone -Title $_.Title -Server $Server
            }
            else {
                #EnggRole and Division won't work yet, since AD schema has not yet been extended. Department is now filling what I intend to be the Division in the future.
                $Splat = @{
                    Identity = $_.Ccid
                    Server = $Server
                }
                $UInfo = $_
                $Attributes | ForEach-Object {
                    $Splat += @{ $_ = $UInfo.$_ }
                }
                
                Set-ADUser @Splat
            }
        }
    }

    end {}
}