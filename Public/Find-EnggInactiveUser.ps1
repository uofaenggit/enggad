﻿<#
.SYNOPSIS
   Retrieve inactive users in Active Directory
.DESCRIPTION
   Retrieve inactive users in Active Directory that have not logged into the domain in the specified amount of time. Allows you to exclude certain
   OU's in your search, based on the regex you provide.

   *NOTE: If a user has never logged in, the LastLogonTimestamp, which is what the Search-ADAccount is using, will be set to 12/31/1600. So if I created
   a user yesterday, and that user hasn't logged on yet, it will get caught by this function unless you use the CreatedBuffer parameter.
.PARAMETER Excluded
   This will be a regex which will be used to exclude the objects returned based on the DistinguishedName
.PARAMETER Days
   This will be the number of days since the object has talked to a domain controller
.PARAMETER CreatedBuffer
   This will allow us to give an allowance for users created this many days ago, but haven't logged in yet
.EXAMPLE
   Find-EnggInactiveUser

   Returns all the users that haven't checked in in over 90 days (the default, but probably a bit short for our purposes)  
.EXAMPLE
   Find-EnggInactiveUser -Excluded "OU=Privileged Accounts,OU=IT,DC=engineering,DC=ualberta,DC=ca" -Days 365 -CreatedBuffer 30

   Returns all the users that haven't checked in in over 365 days, were created over 30 days ago (and haven't logged in yet), and aren't in the following OU's:
         OU=Privileged Accounts,OU=IT,DC=engineering,DC=ualberta,DC=ca
.NOTES
   Author: Curtis D. Oliver
   Created: January 22, 2020
#>
#Requires -Modules ActiveDirectory

function Find-EnggInactiveUser {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Regex to exclude the objects returned based on the DistinguishedName")]
        [regex]$Excluded,
        [Parameter(HelpMessage="Number of days since the object has talked to a DC")]
        [int]$Days = 365,
        [Parameter(HelpMessage="Give an allowance for users created this many days ago, but haven't logged in yet")]
        [int]$CreatedBuffer
    )

    BEGIN {}

    PROCESS {
        if ( $Excluded ) {
            if ( $CreatedBuffer ) {
                Search-ADAccount -AccountInactive -UsersOnly -TimeSpan (New-TimeSpan -Days $Days) | Where-Object { ($_.DistinguishedName -notmatch $Excluded) -and ($_.Created -gt (Get-Date).AddDays(-$CreatedBuffer)) }
            }
            else {
                Search-ADAccount -AccountInactive -UsersOnly -TimeSpan (New-TimeSpan -Days $Days) | Where-Object { $_.DistinguishedName -notmatch $Excluded }
            }
        }
        else {
            if ( $CreatedBuffer ) {
                Search-ADAccount -AccountInactive -UsersOnly -TimeSpan (New-TimeSpan -Days $Days) | Where-Object { $_.Created -gt (Get-Date).AddDays(-$CreatedBuffer) }
            }
            else {
                Search-ADAccount -AccountInactive -UsersOnly -TimeSpan (New-TimeSpan -Days $Days)
            }
        }
    }

    END {}
}