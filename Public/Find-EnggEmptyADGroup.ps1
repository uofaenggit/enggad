﻿<#
.SYNOPSIS
   Find empty groups in domain
.DESCRIPTION
   Find empty groups in domain. Doesn't mean they're not actually used somewhere; just for informational purposes
.PARAMETER SearchBase
   This will typically be a distinguished name of an organizational unit
.EXAMPLE
   Find-EnggEmptyADGroup

   Finds all the groups with no members under the default searchbase "DC=engineering,DC=ualberta,DC=ca"
.EXAMPLE
   Find-EnggEmptyADGroup -SearchBase "CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=engineering,DC=ualberta,DC=ca"

   Finds all the groups with no members under the searchbase "CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=engineering,DC=ualberta,DC=ca"
.NOTES
   Author: Curtis D. Oliver
   Created: January 22, 2020
#>
function Find-EnggEmptyADGroup {
    [CmdletBinding()]
    param (
        [string[]]$SearchBase = "DC=engineering,DC=ualberta,DC=ca"
    )

    BEGIN {}

    PROCESS {
        $SearchBase | ForEach-Object {
            Get-ADGroup -Filter * -SearchBase $_ -Properties Members | Where-Object { 
                ($_.DistinguishedName -notlike "*CN=Builtin,DC=engineering,DC=ualberta,DC=ca") -and (!$_.Members)
            }
        }
    }

    END {}
}