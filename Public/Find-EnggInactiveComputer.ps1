﻿<#
.SYNOPSIS
   Retrieve stale computer objects in Active Directory
.DESCRIPTION
   Retrieve stale computer objects in Active Directory which have not checked into a domain controller in the specified amount of time. Allows you to exclude certain
   OU's in your search, based on the regex you provide.
.PARAMETER Excluded
   This will be a regex which will be used to exclude the objects returned based on the DistinguishedName
.PARAMETER Days
   This will be the number of days since the object has talked to a domain controller
.EXAMPLE
   Find-EnggInactiveComputer

   Returns all the computer objects that haven't checked in in over 90 days (the default)  
.EXAMPLE
   Find-EnggInactiveComputer -Excluded "OU=Servers,DC=engineering,DC=ualberta,DC=ca|OU=Virtual,OU=Workstations,OU=FAC,DC=engineering,DC=ualberta,DC=ca|Linux" -Days 365

   Returns all the computer objects that haven't checked in in over 365 days and aren't in the following OU's:
         OU=Servers,DC=engineering,DC=ualberta,DC=ca
         OU=Virtual,OU=Workstations,OU=FAC,DC=engineering,DC=ualberta,DC=ca
   or where the OU contains "Linux" (we'll leave the Linux machines alone, cause they can be funny)
.NOTES
   Author: Curtis D. Oliver
   Created: January 22, 2020
#>
#Requires -Modules ActiveDirectory

function Find-EnggInactiveComputer {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Regex to exclude the objects returned based on the DistinguishedName")]
        [regex]$Excluded,
        [Parameter(HelpMessage="Number of days since the object has talked to a DC")]
        [int]$Days = 180
    )

    BEGIN {}

    PROCESS {
        if ( $Excluded ) {
            Search-ADAccount -AccountInactive -ComputersOnly -TimeSpan (New-TimeSpan -Days $Days) | Where-Object { $_.DistinguishedName -notmatch $Excluded }
        }
        else {
            Search-ADAccount -AccountInactive -ComputersOnly -TimeSpan (New-TimeSpan -Days $Days)
        }
    }

    END {}
}