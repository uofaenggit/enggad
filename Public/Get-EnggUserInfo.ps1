﻿<#
.SYNOPSIS
   Retrieves user info from https://directory.ualberta.ca
.DESCRIPTION
   Retrieves user info from https://directory.ualberta.ca which we can then use to populate our user attributes in AD.
.PARAMETER Ccid
   The Ccid (Campus Computing ID) paramter is the username. Duh...
.PARAMETER Sleep
   Use this if you want to slow down the queries to the website. 
.EXAMPLE
   Get-EnggUserInfo -Ccid cdoliver

   Returns the user cdoliver's info from the directory. You can then use this to populate/update the object attributes in AD, or compare them
   with the current attributes.
.EXAMPLE
   Get-EnggUserInfo -Ccid cdoliver -Sleep 500

   Does the same as above, but sleeps for 500 milliseconds between each website query. Might want to use this if you are processing thousands of users.
.NOTES
   Author: Curtis D. Oliver
   Created: January 31, 2020
#>
function Get-EnggUserInfo {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true,
                   Position=0,
                   ValueFromPipelineByPropertyName=$true,
                   HelpMessage="It's the username or CCID, dummy. Do I really have to explain this to you? If you don't know what a CCID is, you probably shouldn't be using this function.")]
        [Alias("Name","UserName","Id")]
        [string[]]$Ccid,
        [Parameter(HelpMessage="Use this if you want to slow down the queries to the website")]
        [int]$Sleep
    )

    begin {}

    process {
        $Ccid | ForEach-Object {
            try {
                $Content = (Invoke-WebRequest -Uri "https://directory.ualberta.ca/person/$_" -UseBasicParsing).Content -split "`n"
                $Metadata = $Content | ?{ $_ -match "<meta name=`"ua__dir_" } | trim
                $Properties = (($Metadata -replace '<meta name="ua__dir_','') -replace '" content="') -replace '">'
                $Name = if($Properties -match "name") { ($Properties -match "name")[0] -replace "name" | trim } else {}
                $Title = if($Properties -match "title") { ($Properties -match "title")[0] -replace "title" -replace '&amp;','and' | trim } else {}
                $Department = if($Properties -match "department") { ($Properties -match "department")[0] -replace "department|Faculty of Engineering - | dept" -replace '&amp;','and' | trim } else {}
                $Building = if($Properties -match "building") { (($Properties -match "building")[0] -replace "building") -replace '&amp;','and' | trim } else {}
                $Office = &{ if ( $Content | Select-String '<address style="align-self: flex-end; flex-grow: 1" class="m-0">' ) {
                                ($Content[($Content | Select-String '<address style="align-self: flex-end; flex-grow: 1" class="m-0">').LineNumber] -replace "<br>") -replace '&amp;','and' | trim | join '|'
                             }
                             else {}
                           }
                $Phone = if($Properties -match "phone") { ($Properties -match "phone")[0] -replace "phone" | trim } else {}
                $Email = if($Properties -match "email") { ($Properties -match "email")[0] -replace "email" | trim } else {}
                [PSCustomObject]@{
                    Ccid = $_
                    DisplayName = $Name
                    Description = ""
                    Title = $Title
                    Department = $Department
                    Building = $Building
                    Office = $Office
                    OfficePhone = $Phone
                    Mail = $Email
                }
            }
            catch {
                Write-Error $_.Exception.Message
                Write-Verbose "$($_.TargetObject.RequestUri.AbsoluteUri) returned a 404" #Probably... I'm not looking for other terminating errors :)
            }

            if ( $Sleep ) {
                Start-Sleep -Milliseconds $Sleep
            }
        }    
    }

    end {}
}